﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

using Microsoft.AspNet.SignalR;
using ScoreReSTServer.Models;

namespace ScoreReSTServer.Controllers
{
    /// <summary>
    /// ReST controller for scores
    /// </summary>
    [AuthenticationFilter]
    [EnableCors("*", "*", "*")]
    public class ScoreController : ApiController
    {
        IScorePersistence scorePersistence;

        public ScoreController()
        {
            scorePersistence = new ScorePersistence();
        }

        //public ScoreController(IScorePersistence scorePersistence)
        //{
        //    this.scorePersistence = scorePersistence;
        //}

        // POST: api/Score
        /// <summary>
        /// Inserts a new Score.
        /// </summary>
        /// <param name="value">The Score to insert. 
        /// ID, UserName, Uploaded and DeleteFlag are ignored and populated by the WebApp.</param>
        /// <returns>Http 201 on success, 500 otherwise. Generated ID returned in Location header.</returns>
        public HttpResponseMessage Post([FromBody]Score value)
        {
            long id = scorePersistence.CreateScore(value);
            HttpResponseMessage response;

            if(id != -1)
            {
                response = Request.CreateResponse(HttpStatusCode.Created); // 201
                response.Headers.Location = new Uri(Request.RequestUri, string.Format("score/{0}", id));
                NotifyClients();
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError); // 500
            }

            return response;
        }

        /// <summary>
        /// Sends notice to registered clients via SignalR.
        /// </summary>
        void NotifyClients()
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<ScoreHub>();
            context.Clients.All.broadcastMessage();
        }

        // TODO: this auto-de/serializes the Scores as json.  where is this done?
        // GET: api/Score
        /// <summary>
        /// Gets all Scores.
        /// </summary>
        /// <returns>An ArrayList of all Scores (may be empty) on success; throws Http 500 otherwise.</returns>
        public List<Score> Get()
        {
            List<Score> scores = scorePersistence.RetrieveAllScores();

            if(scores == null)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError); // 500
            }

            return scores;
        }

        // GET: api/Score/5
        /// <summary>
        /// Gets a Score by ID.
        /// </summary>
        /// <param name="id">ID of the Score to retrieve.</param>
        /// <returns>A Score on success; throws Http 404 otherwise.</returns>
        public Score Get(long id)
        {
            Score score = scorePersistence.RetrieveScore(id);

            if(score == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound); // 404
            }

            return score;
        }

        // PUT: api/Score/5
        /// <summary>
        /// Updates a Score by ID.
        /// </summary>
        /// <param name="id">ID of the Score to update.</param>
        /// <param name="value">The Score containing the updated values. All fields must be supplied even those that are unchanged.</param>
        /// <returns>Http 204 on success, 404 otherwise.</returns>
        public HttpResponseMessage Put(long id, [FromBody]Score value)
        {
            HttpResponseMessage response;
            bool success = scorePersistence.UpdateScore(id, value);

            if(success)
            {
                response = Request.CreateResponse(HttpStatusCode.NoContent); // 204
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);  // 404
            }

            return response;
        }

        // DELETE: api/Score/5
        /// <summary>
        /// Deletes a Score by ID.
        /// </summary>
        /// <param name="id">ID of the Score to delete.</param>
        /// <returns>Http 204 on success, 404 otherwise.</returns>
        public HttpResponseMessage Delete(long id)
        {
            bool success = scorePersistence.DeleteScore(id);
            HttpResponseMessage response;

            if(success)
            {
                response = Request.CreateResponse(HttpStatusCode.NoContent); // 204
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);  // 404
            }

            return response;
        }
    }
}
