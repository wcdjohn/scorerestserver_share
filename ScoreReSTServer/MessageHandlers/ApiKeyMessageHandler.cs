﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace ScoreReSTServer.MessageHandlers
{
    public class ApiKeyMessageHandler : DelegatingHandler
    {
        const string ApiKey = "ApiKey";
        const string InvalidApiKey = "Invalid API Key";

        protected override async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage requestMessage, CancellationToken cancellationToken)
        {
            IEnumerable<string> requestHeaders;
            var doesApiKeyExist = requestMessage.Headers.TryGetValues("ApiKey", out requestHeaders);
            if(doesApiKeyExist && requestHeaders.FirstOrDefault().Equals(ApiKey))
            {
                var response = await base.SendAsync(requestMessage, cancellationToken);
                return response;
            }
            else
            {
                return requestMessage.CreateResponse(HttpStatusCode.Forbidden, InvalidApiKey);
            }
        }
    }
}